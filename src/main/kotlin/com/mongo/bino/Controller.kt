package com.mongo.bino

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class AddressController(
    private val service: FeatureService
) {
    @PostMapping
    fun findAllByCoordinates(@RequestBody dto: SearchDto) = service.findAllByCoordinates(dto)

    @PostMapping("save")
    fun add(@RequestBody features: MongoFeature) = service.save(features)

    @PostMapping("reindex")
    fun reindex() = service.reindex()
}