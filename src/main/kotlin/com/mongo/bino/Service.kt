package com.mongo.bino

import com.google.gson.Gson
import com.google.gson.JsonParser
import jdk.nashorn.internal.parser.JSONParser
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Service
import java.io.FileReader
import java.nio.file.Files
import java.nio.file.Paths


interface FeatureService{
    fun findAllByCoordinates(dto: SearchDto): WrapperDto
    fun save(features: MongoFeature): MongoFeature
    fun reindex()
}

@Service
class FeatureServiceImp(
    private val mongoTemplate: MongoTemplate,
    private val featureRepository: FeatureRepository
    ):FeatureService {
    override fun findAllByCoordinates(dto: SearchDto): WrapperDto {
        val query = Query()
        query.addCriteria(Criteria.where("x").gt(dto.xMin).lt(dto.xMax).and("y").gt(dto.yMin).lt(dto.yMax))
        val feature = mongoTemplate.find(query, MongoFeature::class.java).map { Feature(it.type, it.properties, Geometry(type = it.geometry.type, coordinates = it.geometry.coordinates)) }
        return WrapperDto(feature, crs = Crs(type = "name", Properties(name = "urn:ogc:def:crs:OGC:1.3:CRS84")), type = "FeatureCollection", name = "bino")
    }
    override fun save(features: MongoFeature): MongoFeature {
        return featureRepository.save(features)
    }

    override fun reindex() {
        featureRepository.deleteAll()
        val reader = Files.newBufferedReader(Paths.get("bino.json"))
        val parser = JsonParser.parseReader(reader).asJsonObject
        val gson = Gson()
        val json = gson.fromJson(parser, WrapperDto::class.java)
        json.features.forEach { feature ->
            val x = feature.geometry.coordinates[0][0][0][0]
            val y = feature.geometry.coordinates[0][0][0][1]
            var xMin = x
            var xMax = x
            var yMin = y
            var yMax = y
            feature.geometry.coordinates[0][0].forEach {
                if (xMax < it[0]) {
                    xMax = it[0]
                } else if (xMin > it[0]) {
                    xMin = it[0]
                }
                if (yMax < it[1]) {
                    yMax = it[1]
                } else if (yMin > it[1]) {
                    yMin = it[1]
                }
            }
            mongoTemplate.save(MongoFeature(feature.type, feature.properties, feature.geometry, x = (xMax+xMin)/2, y = (yMax+yMin)/2))
        }
    }
}