package com.mongo.bino

import org.springframework.data.mongodb.repository.MongoRepository

interface FeatureRepository : MongoRepository<MongoFeature, String> {
}