package com.mongo.bino

import java.math.BigDecimal

data class SearchDto(
    val xMax: Double,
    val xMin: Double,
    val yMax: Double,
    val yMin: Double
)

data class WrapperDto(
    val features: List<Feature>,
    val type: String,
    val name: String,
    val crs: Crs
)

data class MongoFeature(
    val type: String,
    val properties: Property,
    val geometry: Geometry,
    val x: Double,
    val y: Double
)

data class Feature(
    val type: String,
    val properties: Property,
    val geometry: Geometry
)

data class Geometry(
    val type: String,
    val coordinates: List<List<List<List<Double>>>>
)

data class Property(
    val OBJECTID_1: Long?,
    val OBJECTID_2: Long?,
    val OBJECTID: Long?,
    val SHAPE_Leng: Double?,
    val SUBYEKT: String?,
    val VILOYAT: String?,
    val TUMAN: String?,
    val MAHALLA: String?,
    val KUCHA_NOMI: String?,
    val KADASTR1: String?,
    val Shape_Le_1: Double?,
    val Shape_Le_2: Double?,
    val Shape_Area: Double?,
    val UY_RAQAM: Long?
)

data class Crs(
    val type: String,
    val properties: Properties
)

data class Properties(
    val name: String
)