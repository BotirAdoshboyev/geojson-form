package com.mongo.bino

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BinoApplication

fun main(args: Array<String>) {
    runApplication<BinoApplication>(*args)
}
